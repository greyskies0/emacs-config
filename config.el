;; User info
(setq user-full-name "Maciej Krzyżowski"
      user-mail-address "maciej.krzyzowski9@gmail.com")

;; Theme and font
(setq doom-theme 'doom-tokyo-night)
(setq doom-font (font-spec :family "FantasqueSansM Nerd Font Mono" :size 16))

;; Enabling bold and italic fonts
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t
        doom-themes-padded-modeline t))

;; Cursor settings
(setq evil-insert-state-cursor '(bar "#f7768e")
      evil-normal-state-cursor '(box "#f7768e"))
(blink-cursor-mode 1)

;; Disable some warnings
(setq warning-minimum-level :error)
(setq native-comp-async-report-warnings-errors nil)

;; Org setup
(setq org-directory "~/Dokumenty/org")
(setq org-agenda-files (list "notes.org" "todo.org"))
(setq org-capture-templates
      `(("n" "Notes" entry  (file+headline "notes.org" "Notes")
         ,(concat "* %u => %a\n"
                  "%?%i"))))
(define-key global-map (kbd "C-c c") 'org-capture)
(defun org-capture-note ()
  (interactive)
  (call-interactively 'org-store-link)
  (org-capture nil "n"))
(define-key global-map (kbd "C-c n") 'org-capture-note)

(setq org-agenda-prefix-format
      '(
        (agenda . " %i %-12:c%?-12t% s")
        (todo . " %i %b")
        (tags . " %i %b%-12:c")
        (search . " %i %b%-12:c")
        ))

;; Relative numbers
(setq display-line-numbers-type 'relative)

;; Deferred compilation
(setq native-comp-jit-compilation t)

;; Tab settings
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq-default c-basic-offset 4)

;; Scroll margin
(setq scroll-margin 15)
;; Pixel scroll - emacs 29
(setq pixel-scroll-precision-large-scroll-height 50.0)
(setq pixel-scroll-precision-interpolation-factor 30)
(mouse-wheel-mode -1)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil)            ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't)                  ;; scroll window under mouse
(setq scroll-step 10)                               ;; keyboard scroll one line at a time
(pixel-scroll-precision-mode)

;; Treesitter settings
(global-tree-sitter-mode)
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)

;; Battery indication
(if (file-exists-p "/sys/class/power_supply/BAT0")
    (display-battery-mode))

;; Colouring of hex colours
(rainbow-mode)

;; ESC quits prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Disable irony as syntax checker
(setq-default flycheck-disabled-checkers '(irony))

;; <SPC> r r to replace string
(map! :leader
      :desc "Replace string"
      "r r" #'anzu-query-replace)

(map! :leader
      :desc "Comment selection"
      "t c" #'comment-line)

;; Use C++20 as standard while checking syntax
(add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-language-standard "c++20")))
(add-hook 'c++-mode-hook (lambda () (setq flycheck-clang-language-standard "c++20")))

;; Enable tabnine autocompletion
(add-to-list 'company-backends #'company-tabnine) ; Trigger completion immediately.
(setq company-idle-delay 0) ; Number the candidates (use M-1, M-2 etc to select completions).
(setq company-show-quick-access t)

;; Undo tree
(global-undo-tree-mode)
(setq undo-tree-visualizer-timestamps t)
(map! :leader
      :desc "Visualise the undo tree"
      "t v" #'undo-tree-visualize)
(after! undo-tree
  (setq undo-tree-auto-save-history nil))

;; Start in full-screen
(add-hook 'window-setup-hook 'toggle-frame-maximized t)

(doom/reload-font)
